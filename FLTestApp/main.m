//
//  main.m
//  FLTestApp
//
//  Created by Yasir M Turk on 22/01/2016.
//  Copyright © 2016 Yasir M Turk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
