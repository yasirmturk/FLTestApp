//
//  DetailViewController.h
//  FLTestApp
//
//  Created by Yasir M Turk on 22/01/2016.
//  Copyright © 2016 Yasir M Turk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

